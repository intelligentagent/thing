Thing is an on source 3D-printer with focus on high speed, packability and a great user interface. 

Changes from Thing Rev A0 to A1: 
 - Wrong distance between bolts on 12mm holders.  
 - Missing hole for the power cord.  
 - Missing hole for the radial fan wires.  
 - Wrong distance for all stepper motor mounts.  
 - Position of bolts on top interfere with extruder steppers.  
 - Bolts on forward facing fan duct too high up. Must be moved 5mm down. 
 - Add holes for fan duct in top plate and back plate. 
 - 