TODO:

Spline:
- Test dual extrusion
+ Make Spline PCB 
+ Order Spline PCB
+ New footprint for LED and switches
+ Find solution to Pipe and tooth fastening. (Using e-clip)
+ Make new extruder with smooth shit.
+ Make the other extruder, mirrored. 


Website:
+ Product texts
+ Product pictures
- Move blog
+ Wiki
+ Forum

Manga Screen: 
+ Find producer for manga screen. Ibe/circuitco/top electronics (Sendt mail to CiruitCo)
+ Find manufacturer of capacitive multitouch or use standard HTC


''' Buy '''
- White Acrylic (Formulor)
- Beagle bones
+ Thermistors (Digikey) 
+ Manga Screen A3 Components (Digikey)
+ Spline components (Digikey)
+ Manga Screen A3 PCB
+ Spline PCB
+ Hairspray
+ LM8LUU
+ 12V fan blower
+ Tungsten carbide 0.4mm drill set
+ 20mm flexible tube
- LMF12LUU

Replicape:
+ Assemble PCB
+ Optimize make and braid data.
+ Fix switching noise issue on Replicape.
+ investigate thermocouple
+ Investigate why steppers are noisy. 


Thing Rev A1:
- Find placement for fans - perhaps on XY connections?
- Make new split XY-connections
- mount belt security
+ Mount End stops
- Make new nozzles - waiting for 0.4mm drills
- Feet to absorb vibration - Aliexpress 
+ Mount Dallas temperature sensor in the cold end.
+ Fat pipe on extruder.
+ Update the Thing BOM
+ Make new hot end cartridge holder. 

Thing Rev A2: 
+ Change color to white
+ Wrong distance between bolts on 12mm holders.
- Missing hole for the power cord.
- Missing hole for the radial fan wires.
- Wrong distance for all stepper motor mounts.
- Position of bolts on top interfere with extruder steppers.
- Bolts on forward facing fan duct too high up. Must be moved 5mm down. - Add holes for fan duct in top plate and back plate. -
- Update the Sketchup file and add end stops for X and Z 
- Find solution to fasten Borosilicate glass. 
- Pipes for wires - better solution
- Acrylic bottom plate - move things up
- Better solution on fan duct. 
- Logo - what to do? - Transparent acrylic with lights? 
- Transparent acrylic on LEDs.
- Cover for display? 
- Cables in the BOM
- Producer of Alumninium
- Consider new hot bed mounting since it gets very hot. 

Thing Rev A3:
- Find out how to manufacture and produce the aluminum parts. (Go to a tradeshow in germany?) (email sent to China) 
- Find out why the heater cartridge quality is so bad.
- Could it be an option to make the acrylic in-house? New employee, but option for color. 
- CE-marking - what to do? 

''' Bugs '''
- Try a model with small details
+ Lining on the wall due to ooze on streches outside of model, perhaps try more retract. 

